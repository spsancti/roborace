#!/usr/bin/env python

"""
A simple echo server
"""

import time
import socket
import numpy as np
import matplotlib.pyplot as plt

host = ''
port = 50000
backlog = 5
size = 1024

def left():
    return "leftIR"
def front():
    return "frontIR"
def right():
    return "rightIR"


def sensorName(argument):
    switcher = {
        0: left,
        1: front,
	2: right,
    }
    # Get the function from switcher dictionary
    func = switcher.get(argument)
    # Execute the function
    return func()

fig = plt.figure()
plt.axis([-100, 100, 0, 100])
plt.ion()
plt.grid(True)
plt.show()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((host,port))
s.listen(backlog)
client, address = s.accept()

print address
while 1:
    data = client.recv(size)
    if data and (data[0] == 'Z'):
        data = data[1:]        
        print data
        valueArr = data.split(' ')
    
        plt.clf()
        plt.axis([-100, 100, 0, 100])
        plt.grid(True)
    
        for z in range(len(valueArr)):
            print sensorName(z), float(valueArr[z])
            x = float(valueArr[z]) * 0.7071 - 6.5 #5

            if (z == 2): x = -x
            if (z == 1): x = 0	
            y = float(valueArr[z])
            if (z != 1): y = y * 0.7071
	
            plt.text(x, y-10, sensorName(z), fontsize=6, bbox=dict(edgecolor='w', color='w'))

            plt.scatter(x, y)
        plt.draw()
        plt.pause(0.0001)
input("Press Enter to continue...")

