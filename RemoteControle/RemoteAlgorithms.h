#ifndef REMOTEALGORITHMS_H_
#define REMOTEALGORITHMS_H_

#include "BaseAlgoCase.h"
#include "Engine.h"

class RemoteAlgorithms : public BaseAlgoCase{
private:
    static Engine * _en;
    static bool _enabled;
public:
    static void setEngine(Engine * en);
    static void setSpeed(float value);
    static void setSteer(float value);
    static void toggle(float value);
    static bool isEnabled();
};
#endif
