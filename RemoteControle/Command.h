template <class T>
class Command {
public:
    Command(char C, void (T::*func)(float), T *obj) {
        _c = C;
        _func = func;
        _obj = obj;
    }

    Command(char C, void (*func)(float)) {
        _c = C;
        _funcS = func;
        _obj = 0;
    }

   bool exec(float val) {
       if (_obj)
           (_obj->*_func)(val);
       else
           (*_funcS)(val);
   }

    char c() {
        return _c;
    }

    friend bool operator ==( Command &command, char C) {
        return (command.c() == C);
    }

private:
    char _c;
    void (T::*_func)(float);
    void (*_funcS)(float);
    T * _obj;
};
