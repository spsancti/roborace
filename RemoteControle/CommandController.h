#include "Command.h"
#include <vector>
#include <algorithm>
#include <Engine.h>
#include "BaseAlgoCase.h"

using namespace std;

class CommandController {
public:
    CommandController();
    ~CommandController();

    template <class T>
    bool add(char C, void (T::*func)(float), T *obj) {
        _commands.push_back(Command<BaseAlgoCase>(C,func,obj));
    }

    bool add(char C, void (*func)(float)) {
        _commands.push_back(Command<BaseAlgoCase>(C,func));
    }

    bool exec(char C, float value) {
        std::vector<Command <BaseAlgoCase> >::iterator it;

        it = find (_commands.begin(), _commands.end(), C);
        if (it != _commands.end()) {
            it->exec(value);
            return true;
        }
        else
            return false;
    }

private:
    std::vector< Command<BaseAlgoCase> > _commands;
};
