#include "RemoteAlgorithms.h"

Engine * RemoteAlgorithms::_en = 0;
bool RemoteAlgorithms::_enabled = false;

void RemoteAlgorithms::setEngine(Engine *en)
{
    _en = en;
}

void RemoteAlgorithms::setSpeed(float value)
{
    _en->setSpeed(value);
}

void RemoteAlgorithms::setSteer(float value)
{
    _en->steer(value);
}

void RemoteAlgorithms::toggle(float value)
{
    _enabled = !_enabled;
    _en->setSpeed(0);
}

bool RemoteAlgorithms::isEnabled()
{
    return _enabled;
}
