#include "OrientationComputer.h"

OrientationComputer::OrientationComputer() : imu(LSM303_SDA, LSM303_SCL)
{
	dt = 0.0f;
	beta = 2.0f * 0.15f;	
	quat.w = 1;
}

void OrientationComputer::init(int accSensitivity, int gyroSensitivity)
{
	if(accSensitivity < 0 || accSensitivity > 3) error("OrientationComputer::init() wrong parameter");
	if(gyroSensitivity < 0 || gyroSensitivity > 3) error("OrientationComputer::init() wrong parameter");
	
	uint8_t whoami = imu.readByte(MPU6050_ADDRESS, WHO_AM_I_MPU6050);  // Read WHO_AM_I register for MPU-6050
	if (whoami != 0x68) 
	{
		SS::get().printf("OrientationComputer: IMU failed to connect!\n\r");
	}
	
	imu.selfTest(acc, gyro); 
	
	SS::get().printf("OrientationComputer: x-axis self test: acceleration trim within : %f", acc.x); SS::get().printf("% of factory value \n\r");
	SS::get().printf("OrientationComputer: y-axis self test: acceleration trim within : %f", acc.y); SS::get().printf("% of factory value \n\r");
	SS::get().printf("OrientationComputer: z-axis self test: acceleration trim within : %f", acc.z); SS::get().printf("% of factory value \n\r");
	SS::get().printf("OrientationComputer: x-axis self test: gyration trim within : %f", gyro.x); SS::get().printf("% of factory value \n\r");
	SS::get().printf("OrientationComputer: y-axis self test: gyration trim within : %f", gyro.y); SS::get().printf("% of factory value \n\r");
	SS::get().printf("OrientationComputer: z-axis self test: gyration trim within : %f", gyro.z); SS::get().printf("% of factory value \n\r");
	

	if(acc.x < 1.0f && acc.y < 1.0f && acc.z < 1.0f && gyro.x < 1.0f && gyro.y < 1.0f && gyro.z < 1.0f) 
	{
		imu.reset();
		imu.calibrate(gyroBias, accBias);
		imu.setGScale(gyroSensitivity);
		imu.setAScale(accSensitivity);
		
		imu.init(); 
		SS::get().printf("OrientationComputer:: MPU6050 initialized!\n\r"); // Initialize device for active mode read of acclerometer, gyroscope, and temperature
	}
	else
		SS::get().printf("OrientationComputer: MPU6050 did not the pass self-test!\n\r");      
	
	t.start();
}

bool OrientationComputer::isDataReady()
{
	return (bool)(imu.readByte(MPU6050_ADDRESS, INT_STATUS) & 0x01);
}

void OrientationComputer::update()
{
	if(!isDataReady()) return;
	
	updateDt();
	updateIMUAtCarFrame();
	updateQuaternion();
}

void OrientationComputer::updateIMUAtCarFrame()
{
	imu.readAccelData(acc);
	imu.readGyroData(gyro);

	//magic! we need to translate rotated gyroaccel into car frame
	carAcc.x = acc.y;
	carAcc.y = -acc.x;
	carAcc.z = acc.z;
	
	carGyro.x = gyro.y;
	carGyro.y = -gyro.x;
	carGyro.z = -gyro.z;
}

void OrientationComputer::updateQuaternion() 
{
	float gx = carGyro.x / RAD2DEG;
	float gy = carGyro.y / RAD2DEG;
	float gz = carGyro.z / RAD2DEG;
	float ax = carAcc.x;
	float ay = carAcc.y;
	float az = carAcc.z;

	float q0 = quat.w;
	float q1 = quat.v.x;
	float q2 = quat.v.y;
	float q3 = quat.v.z;
	
	float recipNorm;
	float s0, s1, s2, s3;
	float qDot1, qDot2, qDot3, qDot4;
	float _2q0, _2q1, _2q2, _2q3, _4q0, _4q1, _4q2 ,_8q1, _8q2, q0q0, q1q1, q2q2, q3q3;

	// Rate of change of quaternion from gyroscope
	qDot1 = 0.5f * (-q1 * gx - q2 * gy - q3 * gz);
	qDot2 = 0.5f * (q0 * gx + q2 * gz - q3 * gy);
	qDot3 = 0.5f * (q0 * gy - q1 * gz + q3 * gx);
	qDot4 = 0.5f * (q0 * gz + q1 * gy - q2 * gx);

	// Coimute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
	if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))) {

		// Normalise accelerometer measurement
		recipNorm = invSqrt(ax * ax + ay * ay + az * az);
		ax *= recipNorm;
		ay *= recipNorm;
		az *= recipNorm;   

		// Auxiliary variables to avoid repeated arithmetic
		_2q0 = 2.0f * q0;
		_2q1 = 2.0f * q1;
		_2q2 = 2.0f * q2;
		_2q3 = 2.0f * q3;
		_4q0 = 4.0f * q0;
		_4q1 = 4.0f * q1;
		_4q2 = 4.0f * q2;
		_8q1 = 8.0f * q1;
		_8q2 = 8.0f * q2;
		q0q0 = q0 * q0;
		q1q1 = q1 * q1;
		q2q2 = q2 * q2;
		q3q3 = q3 * q3;

		// Gradient decent algorithm corrective step
		s0 = _4q0 * q2q2 + _2q2 * ax + _4q0 * q1q1 - _2q1 * ay;
		s1 = _4q1 * q3q3 - _2q3 * ax + 4.0f * q0q0 * q1 - _2q0 * ay - _4q1 + _8q1 * q1q1 + _8q1 * q2q2 + _4q1 * az;
		s2 = 4.0f * q0q0 * q2 + _2q0 * ax + _4q2 * q3q3 - _2q3 * ay - _4q2 + _8q2 * q1q1 + _8q2 * q2q2 + _4q2 * az;
		s3 = 4.0f * q1q1 * q3 - _2q1 * ax + 4.0f * q2q2 * q3 - _2q2 * ay;
		recipNorm = invSqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3); // normalise step magnitude
		s0 *= recipNorm;
		s1 *= recipNorm;
		s2 *= recipNorm;
		s3 *= recipNorm;

		// Apply feedback step
		qDot1 -= beta * s0;
		qDot2 -= beta * s1;
		qDot3 -= beta * s2;
		qDot4 -= beta * s3;
	}

	// Integrate rate of change of quaternion to yield quaternion
	q0 += qDot1 * dt;
	q1 += qDot2 * dt;
	q2 += qDot3 * dt;
	q3 += qDot4 * dt;

	// Normalise quaternion
	recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
	q0 *= recipNorm;
	q1 *= recipNorm;
	q2 *= recipNorm;
	q3 *= recipNorm;
	
	quat.w = q0;
	quat.v.x = q1;
	quat.v.y = q2;
	quat.v.z = q3;	
}

void OrientationComputer::updateDt() 
{
	dt = t.read();
	t.reset();
}

void OrientationComputer::getAccAtCarFrame (Vector3 &data)
{
	data = carAcc;
}

void OrientationComputer::getGyroAtCarFrame(Vector3 &data)
{
	data = carGyro;
}

float OrientationComputer::getYawRate()
{
	return carGyro.z;
}

float OrientationComputer::getPitchRate()
{
	return carGyro.x;
}

float OrientationComputer::getRollRate()
{
	return carGyro.y;
}

float OrientationComputer::getAccelerationX()
{
	return carAcc.x;
}
float OrientationComputer::getAccelerationY()
{	
	return carAcc.y;
}
float OrientationComputer::getAccelerationZ()
{
	return carAcc.z;
}

void OrientationComputer::getYawPitchRoll(Vector3 &data)
{
	data = Vector3(getYaw(), getPitch(), getRoll());
}

float OrientationComputer::getPitch() 
{
	return atan2(2 * (quat.w * quat.v.x + quat.v.y * quat.v.z), 1 - 2 * (quat.v.x * quat.v.x + quat.v.y * quat.v.y));
}

float OrientationComputer::getYaw() 
{
	return atan2(2 * (quat.w * quat.v.z + quat.v.x * quat.v.y), 1 - 2 * (quat.v.y * quat.v.y + quat.v.z * quat.v.z));
}

float OrientationComputer::getRoll() 
{
	return asin(2 * quat.w * quat.v.y - quat.v.z * quat.v.x);
}

Quaternion OrientationComputer::getQuaternion() 
{
	return Quaternion(quat);
}

float OrientationComputer::invSqrt(float x){
   uint32_t i = 0x5F1F1412 - (*(uint32_t*)&x >> 1);
   float tmp = *(float*)&i;
   return tmp * (1.69000231f - 0.714158168f * x * tmp * tmp);
}
