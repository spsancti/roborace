#if !defined(_ORIENTATIONCOMPUTER_H)
#define _ORIENTATIONCOMPUTER_H

#include "Quaternion.h"
#include "MPU6050.h"
#include "mbed.h"
#include "SerialSingleton.h"

class OrientationComputer {
	Timer t;
	MPU6050 imu;

	Quaternion quat;
	Vector3 acc, gyro;
	Vector3 accBias, gyroBias;
	Vector3 carAcc, carGyro;
	
	float beta; //Madgwick IMU proportional gain 	
	float dt;

public: 
	OrientationComputer();
	
	void init(int accSensitivity, int gyroSensitivity);
	bool isDataReady();
	void update();

	void getAccAtCarFrame (Vector3 &data);
	void getGyroAtCarFrame(Vector3 &data);
	
	float getYawRate();
	float getPitchRate();
	float getRollRate();

	float getAccelerationX();
	float getAccelerationY();
	float getAccelerationZ();

	void getYawPitchRoll(Vector3 &data);
	float getPitch();
	float getYaw();
	float getRoll();

	Quaternion getQuaternion();

private:
	void updateIMUAtCarFrame();
	void updateQuaternion();
	void updateDt();

	float invSqrt(float x);
};


#endif  //_ORIENTATIONCOMPUTER_H