/* NeatGUI Library
 * Copyright (c) 2014 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ProgressBar.h"

ProgressBar::ProgressBar(int x, int y, int w, int h, Font *fnt) : Control(x, y, w, h)
{
    m_Value = 0.0;
    text(NULL);
    font(fnt);
    border(1);
    padding(1);
    foreColor(0xFFFFFFFF);
    backColor(0xFF000000);
}

float ProgressBar::value()
{
    //Return the current value
    return m_Value;
}

void ProgressBar::value(float v)
{
    //Update the value with range-limiting
    if (v < 0)
        m_Value = 0;
    else if (v > 1.0f)
        m_Value = 1.0f;
    else
        m_Value = v;

    //Force a repaint
    invalidate();
}

void ProgressBar::paint(Canvas* canvas)
{
    //Paint the base class
    Control::paint(canvas);

    //Draw the progress bar
    canvas->fillRect(contentPosX(), contentPosY(), contentWidth() * m_Value, contentHeight(), foreColor());
}
