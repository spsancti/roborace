/* NeatGUI Library
 * Copyright (c) 2014 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PROGRESS_BAR_H
#define PROGRESS_BAR_H

#include "mbed.h"
#include "Control.h"
#include "Font.h"

/** ProgressBar class.
 *  Used to a percentage as a bar.
 */
class ProgressBar : public Control
{
public:
    /** Create a ProgressBar object with the specified size, position, and font
     *
     * @param x The X coordinate of the ProgressBar.
     * @param y The Y coordinate of the ProgressBar.
     * @param w The width of the ProgressBar.
     * @param h The height of the ProgressBar.
     * @param fnt The font to draw text with.
     */
    ProgressBar(int x, int y, int w, int h, Font *fnt);

    /** Get the current value of the ProgressBar
     *
     * @returns The current value as a percentage (0.0 to 1.0).
     */
    float value();

    /** Set the value of the ProgressBar
     *
     * @param v The new progress bar value as a percentage (0.0 to 1.0).
     */
    void value(float v);

    /** Paint the ProgressBar on the specified canvas
    *
    * @param canvas Pointer to the canvas to paint on.
    */
    virtual void paint(Canvas* canvas);

protected:
    //Internal variables
    float m_Value;
};

#endif
