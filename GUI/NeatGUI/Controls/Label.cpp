/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Label.h"

Label::Label(int x, int y, int w, int h, Font *fnt) : Control(x, y, w, h)
{
    text(NULL);
    font(fnt);
    foreColor(0xFFFFFFFF);
    backColor(0xFF000000);
}

void Label::paint(Canvas* canvas)
{
    //Paint the base class
    Control::paint(canvas);

    //Draw the text if there is any
    if (text() != NULL && font() != NULL)
        canvas->drawString(text(), font(), contentPosX(), contentPosY(), contentWidth(), contentHeight());
}
