/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LABEL_H
#define LABEL_H

#include "mbed.h"
#include "Control.h"
#include "Font.h"

/** Label class.
 *  Used to display strings.
 */
class Label : public Control
{
public:
    /** Create a Label object with the specified size, position, and font
     *
     * @param x The X coordinate of the Label.
     * @param y The Y coordinate of the Label.
     * @param w The width of the Label.
     * @param h The height of the Label.
     * @param fnt The font to draw text with.
     */
    Label(int x, int y, int w, int h, Font *fnt);

    /** Paint the Label on the specified canvas
    *
    * @param canvas Pointer to the canvas to paint on.
    */
    virtual void paint(Canvas* canvas);
};

#endif
