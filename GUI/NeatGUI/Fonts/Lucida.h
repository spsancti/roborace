#ifndef LUCIDA_H_
#define LUCIDA_H_

#include "BitmapDotFactory.h"

extern const char lucidaBitmaps[];
extern const FONT_INFO lucidaFontInfo;
extern const FONT_CHAR_INFO lucidaDescriptors[];

#endif
