/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Font.h"

Font::Font(const char table[], const FONT_CHAR_INFO descriptors[], const FONT_INFO* font_info)
{
    m_FontTable = table;    
		m_Descriptors = descriptors;
		m_Fontinfo = font_info;
}

BitmapImage Font::glyph(char c)
{
	FONT_CHAR_INFO data = m_Descriptors[c - m_Fontinfo->startChar];
	
	return BitmapImage(m_FontTable+data.offset, data.widthBits, height());
}

int Font::height()
{
    return m_Fontinfo->heightPages;
}

int Font::measureString(const char* str)
{
    int i = 0;
    int slen = 0;

    //Find the length in pixels of the whole string
    while (str[i] != NULL) {
        slen += glyph(*str).width();
        i++;
    }

    return slen;
}

int Font::measureWord(const char* str)
{
    int i = 0;
    int wlen = 0;

    //Find the length in pixels of the next word (separated by whitespace)
    while ((str[i] > ' ') && (str[i] <= 0x7E)) {
        wlen += glyph(*str).width();
        i++;
    }

    return wlen;
}
