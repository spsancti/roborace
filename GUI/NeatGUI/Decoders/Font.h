/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FONT_H
#define FONT_H

#include "mbed.h"
#include "BitmapImage.h"
#include "BitmapDotFactory.h"

/** Font class.
 *  Used to access font data in an external table.
 */
class Font
{
public:
    /** Create a white Font object
     *
     * @param table Pointer to the font table.
     */
    Font(const char* table, const FONT_CHAR_INFO* descriptors, const FONT_INFO* font_info);
  

    /** Get the glyph Image for the specified character
     *
     * @returns The character glyph Image.
     */
    BitmapImage glyph(char c);

     /** Get the height of the Font glyphs
     *
     * @returns The height of the Font glyphs.
     */
    int height();

    /** Measures the width of a string in pixels if drawn with this font
     *
     * @param str Pointer to the string to measure.
     *
     * @returns The width of the string in pixels.
     */
    int measureString(const char* str);

    /** Measures the width of the next word in a string (separated by whitespace) in pixels if drawn with this font
     *
     * @param str Pointer to the string to measure.
     *
     * @returns The width of the next word in pixels.
     */
    int measureWord(const char* str);

protected:
    const char* m_FontTable;
		const FONT_CHAR_INFO* m_Descriptors;
		const FONT_INFO * m_Fontinfo;

};

#endif
