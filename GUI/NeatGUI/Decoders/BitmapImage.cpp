/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "BitmapImage.h"

BitmapImage::BitmapImage(const char* table) : Image(table[0] * 0xFF + table[1], table[2] * 0xFF + table[3])
{
    m_ImageTable = table;
    m_FgColor = 0xFFFFFFFF;
    m_BgColor = 0x00000000;
}

BitmapImage::BitmapImage(const char* table, int width, int height) : Image(width, height)
{
    m_ImageTable = table;		
    m_FgColor = 0xFFFFFFFF;
    m_BgColor = 0x00000000;
}

BitmapImage::BitmapImage(const char* table, unsigned int fg_color) : Image(table[0] * 0xFF + table[1], table[2] * 0xFF + table[3])
{
    m_ImageTable = table;
    m_FgColor = fg_color;
    m_BgColor = 0x00000000;
}

BitmapImage::BitmapImage(const char* table, unsigned int fg_color, unsigned int bg_color) : Image(table[0] * 0xFF + table[1], table[2] * 0xFF + table[3])
{
    m_ImageTable = table;
    m_FgColor = fg_color;
    m_BgColor = bg_color;
}

unsigned int BitmapImage::pixel(int x, int y)
{
    int addr;
    int mask;
    int bpl;

    //Range check the pixel request
    if ((x < 0) || (y < 0) || (x >= width()) || (y >= height())) 
			return 0x000000;

    //Get the pixel address
    bpl = bytesPerLine();
    mask = 0x80 >> (x % 8);
    addr = y * bpl + (x / 8);

    //Return the appropriate color
    if (m_ImageTable[addr] & mask)
        return m_FgColor;
    else
        return m_BgColor;
}

unsigned int BitmapImage::foreground()
{
    return m_FgColor;
}

void BitmapImage::foreground(unsigned int c)
{
    m_FgColor = c;
}

unsigned int BitmapImage::background()
{
    return m_BgColor;
}

void BitmapImage::background(unsigned int c)
{
    m_BgColor = c;
}

int BitmapImage::bytesPerLine()
{
    int bpl;

    //Determine the bytes per line
    bpl = width() / 8;
    if ((width() % 8) != 0) bpl++;

    //Return the bytes per line
    return bpl;
}
