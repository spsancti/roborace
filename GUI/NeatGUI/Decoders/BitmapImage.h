/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BITMAP_IMAGE_H
#define BITMAP_IMAGE_H

#include "mbed.h"
#include "Image.h"

/** BitmapImage class.
 *  Used to access a monochrome bitmap stored in an external table.
 */
class BitmapImage : public Image
{
public:
    /** Create a BitmapImage object from the specified image table with a white foreground, and an alpha background
     *
     * @param table Pointer to the image table.
     */
    BitmapImage(const char* table);


		/** Create a BitmapImage object from the specified image table with the specified size
     *
     * @param table Pointer to the image table.
     * @param width The width of the image.
     * @param height The height of the image.
     */
		BitmapImage(const char* table, int width, int height);

    /** Create a BitmapImage object from the specified image table with the specified foreground, and an alpha background
     *
     * @param table Pointer to the image table.
     * @param fg_color The foreground color as a 32-bit ARGB value.
     */
    BitmapImage(const char* table, unsigned int fg_color);

    /** Create a BitmapImage object from the specified image table with the specified foreground and background colors
     *
     * @param table Pointer to the image table.
     * @param fg_color The foreground color as a 32-bit ARGB value.
     * @param bg_color The background color as a 32-bit ARGB value.
     */
    BitmapImage(const char* table, unsigned int fg_color, unsigned int bg_color);

    /** Get the pixel at the specified coordinates
    *
    * @param x The X coordinate.
    * @param y The Y coordinate.
    *
    * @returns The color of the pixel as a 32-bit ARGB value.
    */
    virtual unsigned int pixel(int x, int y);

    /** Get the foreground color
    *
    * @returns The foreground color as a 32-bit ARGB value.
    */
    unsigned int foreground();

    /** Set the foreground color
    *
    * @param c The new foreground color as a 32-bit ARGB value.
    */
    void foreground(unsigned int c);

    /** Get the background color
    *
    * @returns The background color as a 32-bit ARGB value.
    */
    unsigned int background();

    /** Set the background color
    *
    * @param c The new background color as a 32-bit ARGB value.
    */
    void background(unsigned int c);

protected:
    //Pointer to the image table
    const char* m_ImageTable;

    //The foreground and background colors
    unsigned int m_FgColor, m_BgColor;

    int bytesPerLine();
};

#endif
