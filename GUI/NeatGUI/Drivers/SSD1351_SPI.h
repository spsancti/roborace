/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SSD1351_SPI_H
#define SSD1351_SPI_H

#include "mbed.h"
#include "Display.h"

/** SSD1351_SPI class.
 *  Used for controlling an SSD1351-based OLED display connected to SPI.
 */
class SSD1351_SPI : public Display
{
public:

    /** Create an SSD1351 object connected to the specified SPI pins with the specified /CS and DC pins
     *
     * @param mosi The SPI data out pin.
     * @param miso The SPI data in pin.
     * @param sclk The SPI clock pin.
     * @param cs The SPI chip select pin.
     * @param dc The data/command pin.
     */
    SSD1351_SPI(PinName mosi, PinName miso, PinName sclk, PinName cs, PinName dc);

    /** Probe for the SSD1351 and initialize it if present
     *
     * @returns
     *   'true' if the device exists on the bus,
     *   'false' if the device doesn't exist on the bus.
     */
    virtual bool open();

    /** Send the buffer to the SSD1351
     */
    virtual void flush();

    /** Get the current state of the SSD1351
     *
     * @returns The current state as a Display::State enum.
     */
    virtual Display::State state();

    /** Set the state of the SSD1351
     *
     * @param mode The new state as a Display::State enum.
     */
    virtual void state(State s);

    /** Draw a single pixel at the specified coordinates
    *
    * @param x The X coordinate.
    * @param y The Y coordinate.
    * @param c The color of the pixel as a 32-bit ARGB value.
    */
    virtual void drawPixel(int x, int y, unsigned int c);

private:
    //Commands
    enum Command {
        CMD_SETCOLUMN       = 0x15,
        CMD_SETROW          = 0x75,
        CMD_WRITERAM        = 0x5C,
        CMD_READRAM         = 0x5D,
        CMD_SETREMAP        = 0xA0,
        CMD_STARTLINE       = 0xA1,
        CMD_DISPLAYOFFSET   = 0xA2,
        CMD_DISPLAYALLOFF   = 0xA4,
        CMD_DISPLAYALLON    = 0xA5,
        CMD_NORMALDISPLAY   = 0xA6,
        CMD_INVERTDISPLAY   = 0xA7,
        CMD_FUNCTIONSELECT  = 0xAB,
        CMD_DISPLAYOFF      = 0xAE,
        CMD_DISPLAYON       = 0xAF,
        CMD_PRECHARGE       = 0xB1,
        CMD_DISPLAYENHANCE  = 0xB2,
        CMD_CLOCKDIV        = 0xB3,
        CMD_SETVSL          = 0xB4,
        CMD_SETGPIO         = 0xB5,
        CMD_PRECHARGE2      = 0xB6,
        CMD_SETGRAY         = 0xB8,
        CMD_USELUT          = 0xB9,
        CMD_PRECHARGELEVEL  = 0xBB,
        CMD_VCOMH           = 0xBE,
        CMD_CONTRASTABC     = 0xC1,
        CMD_CONTRASTMASTER  = 0xC7,
        CMD_MUXRATIO        = 0xCA,
        CMD_COMMANDLOCK     = 0xFD,
        CMD_HORIZSCROLL     = 0x96,
        CMD_STOPSCROLL      = 0x9E,
        CMD_STARTSCROLL     = 0x9F
    };

    //SPI interface variables
    SPI m_SPI;
    DigitalOut m_CS;
    DigitalOut m_DC;

    //Caching variables
    int m_StartX;
    int m_StartY;
    int m_CursX;
    int m_CursY;
    char m_Cache[1024];
    int m_CacheIndex;

    //Command and data helpers
    void writeCommand(char command);
    void writeData(char data);
};

#endif
