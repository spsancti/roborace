/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ILI9341_H
#define ILI9241_H

#include "mbed.h"
#include "Display.h"

/** ILI9341 class.
 *  Used for controlling an ILI9341-based TFT display.
 */
class ILI9341 : public Display
{
public:
    /** Create an ILI9341 object connected to the specified SPI pins with the specified /CS and DC pins
     *
     * @param mosi The SPI data out pin.
     * @param miso The SPI data in pin.
     * @param sclk The SPI clock pin.
     * @param sclk The SPI chip select pin.
     * @param sclk The data/command pin.
     */
    ILI9341(PinName mosi, PinName miso, PinName sclk, PinName cs, PinName dc);

    /** Probe for the ILI9341 and initialize it if present
     *
     * @returns
     *   'true' if the device exists on the bus,
     *   'false' if the device doesn't exist on the bus.
     */
    virtual bool open();

    /** Send the buffer to the ILI9341
     */
    virtual void flush();

    /** Get the current state of the ILI9341
     *
     * @returns The current state as a Display::State enum.
     */
    virtual Display::State state();

    /** Set the state of the ILI9341
     *
     * @param mode The new state as a Display::State enum.
     */
    virtual void state(State s);

    //void display();

    /** Draw a single pixel at the specified coordinates
    *
    * @param x The X coordinate.
    * @param y The Y coordinate.
    * @param c The color of the pixel as a 32-bit ARGB value.
    */
    virtual void drawPixel(int x, int y, unsigned int c);

private:
    //Commands
    /*enum Command {
        CMD_SETCONTRAST                             = 0x81,
        CMD_DISPLAYALLON_RESUME                     = 0xA4,
        CMD_DISPLAYALLON                            = 0xA5,
        CMD_NORMALDISPLAY                           = 0xA6,
        CMD_INVERTDISPLAY                           = 0xA7,
        CMD_DISPLAYOFF                              = 0xAE,
        CMD_DISPLAYON                               = 0xAF,
        CMD_SETDISPLAYOFFSET                        = 0xD3,
        CMD_SETCOMPINS                              = 0xDA,
        CMD_SETVCOMDETECT                           = 0xDB,
        CMD_SETDISPLAYCLOCKDIV                      = 0xD5,
        CMD_SETPRECHARGE                            = 0xD9,
        CMD_SETMULTIPLEX                            = 0xA8,
        CMD_SETLOWCOLUMN                            = 0x00,
        CMD_SETHIGHCOLUMN                           = 0x10,
        CMD_SETSTARTLINE                            = 0x40,
        CMD_MEMORYMODE                              = 0x20,
        CMD_COMSCANINC                              = 0xC0,
        CMD_COMSCANDEC                              = 0xC8,
        CMD_SEGREMAP                                = 0xA0,
        CMD_CHARGEPUMP                              = 0x8D,
        CMD_CHARGEPUMPON                            = 0x14,
        CMD_CHARGEPUMPOFF                           = 0x10,
        CMD_ACTIVATE_SCROLL                         = 0x2F,
        CMD_DEACTIVATE_SCROLL                       = 0x2E,
        CMD_SET_VERTICAL_SCROLL_AREA                = 0xA3,
        CMD_RIGHT_HORIZONTAL_SCROLL                 = 0x26,
        CMD_LEFT_HORIZONTAL_SCROLL                  = 0x27,
        CMD_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL    = 0x29,
        CMD_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL     = 0x2A
    };*/

    //Interface variables
    SPI m_SPI;
    DigitalOut m_CS;
    DigitalOut m_DC;

    //Command and data helpers
    void writeCommand(char command);
    void writeData8(char data);
    void writeData16(unsigned short data);
};

#endif
