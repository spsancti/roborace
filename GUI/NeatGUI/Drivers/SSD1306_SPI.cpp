/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "SSD1306_SPI.h"

SSD1306_SPI::SSD1306_SPI(PinName mosi, PinName miso, PinName sclk, PinName cs, PinName dc) : Display(128, 64), m_SPI(mosi, miso, sclk), m_CS(cs), m_DC(dc)
{
    //Deselect the display
    m_CS = 1;

    //Set the SPI format to 8 bit data, high steady state clock, second edge capture
    m_SPI.format(8, 3);

    //Set the SPI frequency to 10MHz
    m_SPI.frequency(10000000);
}

bool SSD1306_SPI::open()
{
    //Init sequence for 128x64 OLED module
    writeCommand(CMD_DISPLAYOFF);
    writeCommand(CMD_SETDISPLAYCLOCKDIV);
    writeCommand(0x80);
    writeCommand(CMD_SETMULTIPLEX);
    writeCommand(0x3F);
    writeCommand(CMD_SETDISPLAYOFFSET);
    writeCommand(0x0);
    writeCommand(CMD_SETSTARTLINE | 0x0);
    writeCommand(CMD_CHARGEPUMP);
    writeCommand(CMD_CHARGEPUMPON);
    writeCommand(CMD_MEMORYMODE);
    writeCommand(0x00);
    writeCommand(CMD_SEGREMAP | 0x1);
    writeCommand(CMD_COMSCANDEC);
    writeCommand(CMD_SETCOMPINS);
    writeCommand(0x12);
    writeCommand(CMD_SETCONTRAST);
    writeCommand(0xCF);
    writeCommand(CMD_SETPRECHARGE);
    writeCommand(0xF1);
    writeCommand(CMD_SETVCOMDETECT);
    writeCommand(0x40);
    writeCommand(CMD_DISPLAYALLON_RESUME);
    writeCommand(CMD_NORMALDISPLAY);

    //Return success
    return true;
}

void SSD1306_SPI::flush()
{
    //Select low col 0, hi col 0, line 0
    writeCommand(CMD_SETLOWCOLUMN | 0x0);
    writeCommand(CMD_SETHIGHCOLUMN | 0x0);
    writeCommand(CMD_SETSTARTLINE | 0x0);

    //Set DC to data and select the chip
    m_DC = 1;
    m_CS = 0;

    //Write the entire buffer at once
    for (int i = 0; i < 1024; i++)
        m_SPI.write(m_Buffer[i]);

    //Deselect the chip
    m_CS = 1;
}

Display::State SSD1306_SPI::state()
{
    //Return the base class's state
    return Display::state();
}

void SSD1306_SPI::state(State s)
{
    //Check what the requested state is
    if (s == Display::DISPLAY_ON) {
        //Turn the display on
        writeCommand(CMD_DISPLAYON);
    } else if (s == Display::DISPLAY_OFF) {
        //Turn the display off
        writeCommand(CMD_DISPLAYOFF);
    }

    //Update the base class
    Display::state(s);
}

void SSD1306_SPI::drawPixel(int x, int y, unsigned int c)
{
    //Range check the pixel
    if ((x < 0) || (x >= width()) || (y < 0) || (y >= height()))
        return;

    //Make sure the color is fully opaque
    if ((c >> 24) != 255)
        return;

    //Determine the pixel byte index
    unsigned short byteIndex = x + (y / 8) * width();

    //Set or clear the pixel
    if ((c & 0x00FFFFFF) > 0)
        m_Buffer[byteIndex] |= (1 << (y % 8));
    else
        m_Buffer[byteIndex] &= ~(1 << (y % 8));
}

void SSD1306_SPI::writeCommand(char command)
{
    //Set DC to command and select the display
    m_DC = 0;
    m_CS = 0;

    //Write the command byte
    m_SPI.write(command);

    //Deselect the display
    m_CS = 1;
}

void SSD1306_SPI::writeData(char data)
{
    //Set DC to data and select the display
    m_DC = 1;
    m_CS = 0;

    //Write the data byte
    m_SPI.write(data);

    //Deselect the display
    m_CS = 1;
}
