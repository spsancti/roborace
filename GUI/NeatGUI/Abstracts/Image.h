/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef IMAGE_H
#define IMAGE_H

#include "mbed.h"

/** Image abstract class.
 *  Used as a base class for image objects.
 */
class Image
{
public:
    /** Create an Image object with the specified width and height
     *
     * @param w The image width.
     * @param h The image height.
     */
    Image(int w, int h);

    /** Get the pixel at the specified coordinates
    *
    * @param x The X coordinate.
    * @param y The Y coordinate.
    *
    * @returns The color of the pixel as a 32-bit ARGB value.
    */
    virtual unsigned int pixel(int x, int y) = 0;

    /** Get the image width
     *
     * @returns The image width.
     */
    int width();

    /** Get the image height
     *
     * @returns The image height.
     */
    int height();

protected:
    //The image width/height
    int m_Width, m_Height;
};

#endif
