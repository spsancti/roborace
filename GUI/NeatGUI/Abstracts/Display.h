/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DISPLAY_H
#define DISPLAY_H

#include "mbed.h"
#include "Canvas.h"

/** Display abstract class.
 *  Used as a base class for Display objects with 2D drawing capabilites.
 */
class Display : public Canvas
{
public:
    /** Represents the state of the Display
     */
    enum State {
        NOT_INITIALIZED,    /**< Display has not been initialized yet */
        DISPLAY_OFF,        /**< Display is initialized and turned off */
        DISPLAY_ON          /**< Display is initialized and turned on */
    };

    /** Create a Display object with the specified width and height
     *
     * @param w The display width.
     * @param h The display height.
     */
    Display(int w, int h);

    /** Probe for the display controller and initialize it if present
     *
     * @returns
     *   'true' if the device exists on the bus,
     *   'false' if the device doesn't exist on the bus.
     */
    virtual bool open() = 0;

    /** flush any writes to the Display
     */
    virtual void flush();

    /** Get the current state of the Display
     *
     * @returns The current state as a State enum.
     */
    virtual Display::State state();

    /** Set the state of the Display
     *
     * @param mode The new state as a State enum.
     */
    virtual void state(State s);

protected:
    Display::State m_State;
};

#endif
