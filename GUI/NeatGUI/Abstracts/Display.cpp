/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Display.h"

Display::Display(int w, int h) : Canvas(w, h)
{
    m_State = NOT_INITIALIZED;
}

void Display::flush()
{
    //Do nothing by default, the super class should override this if needed
}

Display::State Display::state()
{
    //Return the display's state
    return m_State;
}

void Display::state(State s)
{
    //Set the new state
    m_State = s;
}
