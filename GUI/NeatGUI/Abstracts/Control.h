/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONTROL_H
#define CONTROL_H

#include "mbed.h"
#include "Canvas.h"

/** Control abstract class.
 *  Used as a base class for UI elements.
 */
class Control
{
public:
    /** Create a Control object with the specified size and location
     *
     * @param x The X coordinate of the control.
     * @param y The Y coordinate of the control.
     * @param w The width of the control.
     * @param h The height of the control.
     */
    Control(int x, int y, int w, int h);

    /** Paint the control on the specified canvas and mark as valid again
     *
     * @param canvas Pointer to the canvas to paint on.
     */
    virtual void paint(Canvas *canvas);

    /** Get the X coordinate of the control
     *
     * @returns The X coordinate of the control.
     */
    int posX();

    /** Set the X coordinate of the control
     *
     * @param x The new X coordinate for the control.
     */
    void posX(int x);

    /** Get the Y coordinate of the control
     *
     * @returns The Y coordinate of the control.
     */
    int posY();

    /** Set the Y coordinate of the control
     *
     * @param y The new Y coordinate for the control.
     */
    void posY(int y);

    /** Get the width of the control
     *
     * @returns The width of the control.
     */
    int width();

    /** Set the width of the control
     *
     * @param w The new width for the control.
     */
    void width(int w);

    /** Get the height of the control
     *
     * @returns The height of the control.
     */
    int height();

    /** Set the height of the control
     *
     * @param h The new height for the control.
     */
    void height(int h);

    /** Get the current margin width
     *
     * @returns The current margin width.
     */
    int margin();

    /** Set the margin width
     *
     * @param m The new margin width.
     */
    void margin(int m);

    /** Get the current border width
     *
     * @returns The current border width.
     */
    int border();

    /** Set the border width
     *
     * @param b The new border width.
     */
    void border(int b);

    /** Get the current padding width
     *
     * @returns The current padding width.
     */
    int padding();

    /** Set the padding width
     *
     * @param p The new padding width.
     */
    void padding(int p);

    /** Get the X coordinate of the control's content area
     *
     * @returns The X coordinate of the control's content area.
     */
    int contentPosX();

    /** Get the Y coordinate of the control's content area
     *
     * @returns The Y coordinate of the control's content area.
     */
    int contentPosY();

    /** Get the width of the control's content area
     *
     * @returns The width of the control's content area.
     */
    int contentWidth();

    /** Get the height of the control's content area
     *
     * @returns The height of the control's content area.
     */
    int contentHeight();

    /** Get the foreground color
     *
     * @returns The foreground color as a 32-bit ARGB value.
     */
    unsigned int foreColor();

    /** Set the foreground color
     *
     * @param c The new foreground color as a 32-bit ARGB value.
     */
    void foreColor(unsigned int c);

    /** Get the background color
     *
     * @returns The background color as a 32-bit ARGB value.
     */
    unsigned int backColor();

    /** Set the background color
     *
     * @param c The new background color as a 32-bit ARGB value.
     */
    void backColor(unsigned int c);

    /** Get the current text of the control
     *
     * @returns The current text of the control.
     */
    const char* text();

    /** Set the text of the control
     *
     * @param text The new text.
     */
    void text(const char* text);

    /** Get the current font of the control
     *
     * @returns The current font of the control.
     */
    Font* font();

    /** Set the font of the control
     *
     * @param fnt The new font.
     */
    void font(Font* fnt);

    /** Determine whether the control needs to be repainted
     *
     * @returns Whether or not the control needs to be repainted.
     */
    bool invalid();

    /** Mark this control as invalid
     */
    void invalidate();

protected:
    int m_X, m_Y, m_Width, m_Height, m_Margin, m_Border, m_Padding;
    unsigned int m_FgColor, m_BgColor;
    const char* m_Text;
    Font* m_Font;
    bool m_Invalid;
};

#endif
