/* NeatGUI Library
 * Copyright (c) 2013 Neil Thiessen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Control.h"

Control::Control(int x, int y, int w, int h)
{
    m_X = x;
    m_Y = y;
    m_Width = w;
    m_Height = h;
    m_Margin = 0;
    m_Border = 0;
    m_Padding = 0;
    m_FgColor = 0xFFFFFFFF;
    m_BgColor = 0xFF000000;
    m_Text = NULL;
    m_Font = NULL;
    m_Invalid = true;
}

void Control::paint(Canvas* canvas)
{
    //Fill the control's content and padding area
    canvas->fillRect(contentPosX() - m_Padding, contentPosY() - m_Padding , contentWidth() + m_Padding * 2, contentHeight() + m_Padding * 2, m_BgColor);

    //Check if we need to draw a border
    if (m_Border > 0) {
        //Draw the border
        for (int i = 0; i < border(); i++) {
            canvas->drawRect(m_X + m_Margin + i, m_Y + m_Margin + i, m_Width - 2 * (m_Margin + i), m_Height - 2 * (m_Margin + i), m_FgColor);
        }
    }

    //We're no longer invalid
    m_Invalid = false;
}

int Control::posX()
{
    return m_X;
}

void Control::posX(int x)
{
    //Set the new value
    m_X = x;

    //Force a repaint
    m_Invalid = true;
}

int Control::posY()
{
    return m_Y;
}

void Control::posY(int y)
{
    //Set the new value
    m_Y = y;

    //Force a repaint
    m_Invalid = true;
}

int Control::width()
{
    return m_Width;
}

void Control::width(int w)
{
    //Set the new value
    m_Width = w;

    //Force a repaint
    m_Invalid = true;
}

int Control::height()
{
    return m_Height;
}

void Control::height(int h)
{
    //Set the new value
    m_Height = h;

    //Force a repaint
    m_Invalid = true;
}

int Control::margin()
{
    return m_Margin;
}

void Control::margin(int m)
{
    //Update the value
    m_Margin = m;

    //Need to repaint
    m_Invalid = true;
}

int Control::border()
{
    return m_Border;
}

void Control::border(int b)
{
    //Update the value
    m_Border = b;

    //Need to repaint
    m_Invalid = true;
}

int Control::padding()
{
    return m_Padding;
}

void Control::padding(int p)
{
    //Update the value
    m_Padding = p;

    //Need to repaint
    m_Invalid = true;
}

int Control::contentPosX()
{
    return m_X + m_Margin + m_Border + m_Padding;
}

int Control::contentPosY()
{
    return m_Y + m_Margin + m_Border + m_Padding;
}

int Control::contentWidth()
{
    return m_Width - (m_Margin + m_Border + m_Padding) * 2;
}

int Control::contentHeight()
{
    return m_Height - (m_Margin + m_Border + m_Padding) * 2;
}

unsigned int Control::foreColor()
{
    return m_FgColor;
}

void Control::foreColor(unsigned int c)
{
    //Update the value
    m_FgColor = c;

    //Need to repaint
    m_Invalid = true;
}

unsigned int Control::backColor()
{
    return m_BgColor;
}

void Control::backColor(unsigned int c)
{
    //Update the value
    m_BgColor = c;

    //Need to repaint
    m_Invalid = true;
}

const char* Control::text()
{
    return m_Text;
}

void Control::text(const char* text)
{
    //Update the value
    m_Text = text;

    //Need to repaint
    m_Invalid = true;
}

Font* Control::font()
{
    return m_Font;
}

void Control::font(Font* fnt)
{
    //Update the value
    m_Font = fnt;

    //Need to repaint
    m_Invalid = true;
}

bool Control::invalid()
{
    return m_Invalid;
}

void Control::invalidate()
{
    m_Invalid = true;
}
