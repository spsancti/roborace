#include "MPID.h"

MPID::MPID(float Kp, float Ki, float Kd, float rate):
		_Kp(Kp)
	, _Ki(Ki)
	, _Kd(Kd)
	, _rate(rate)
	, lastOutput(0.0f)
	, e_n(0.0f)
	, e_n_1(0.0f)
	, e_n_2(0.0f)
	, _setpoint(0.0f)
	, out_min(0.0f)
	, out_max(100.0f)
{
		if(rate < 0) use_floating_dt = true;
		else 				 use_floating_dt = false;
}
		
void MPID::setTunings(float Kp, float Ki, float Kd)
{
		_Kp = Kp;
		_Ki = Ki;
		_Kd = Kd;
}
void MPID::setSetpoint(float setPoint)
{
		_setpoint = setPoint;
}

void MPID::setConstrains(float Omin, float Omax)
{
		if(Omin >= Omax) return;
		out_min = Omin;
		out_max = Omax;
}

float MPID::computeOutput(float input)
{
		if(use_floating_dt)
		{
			_rate = t.read();
			t.reset();
		}
		
		if(_rate == 0) return 0;
				
		e_n_2 = e_n_1;
		e_n_1 = e_n;	
		e_n = _setpoint - input;
		
		//if(fabsf(e_n) < 10e-3f)  softReset();
		
		float output = lastOutput
					 + _Kp * (e_n - e_n_1)
					 + _Ki * e_n *_rate
					 + _Kd * (e_n - 2* e_n_1 + e_n_2) / _rate;
			
		lastOutput = output;
	
		if(output < out_min) output = out_min;
		if(output > out_max) output = out_max;

		return output;
}

void MPID::hardReset()
{
		softReset();
		_setpoint = 0.0f;
		out_min = 0.0f;
		out_max = 100.0f;
}

void MPID::softReset()
{
			lastOutput = 0.0f;
			e_n = 0.0f;
			e_n_1 = 0.0f;
			e_n_2 = 0.0f;
}
