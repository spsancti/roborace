#ifndef MPID_H
#define MPID_H
#include "mbed.h"

class MPID
{
	private:
		float _Kp, _Ki, _Kd;
		float _rate;
		float lastOutput;
		float e_n, e_n_1, e_n_2;
		float _setpoint;		
		float out_min, out_max;
	
		bool use_floating_dt;
	
	private:
		Timer t;
	
	public: 
		MPID(float Kp, float Ki, float Kd, float rate);
		
		void setTunings(float Kp, float Ki, float Td);
		void setSetpoint(float setPoint);
		void setConstrains(float Omin, float Omax);
		float computeOutput(float input);	
	
		void hardReset();
		void softReset();
};


#endif //MPID_H
