#include <iostream>
#include <Windows.h>
#include "Gamepad.h"
#include "RS232.h"
#include <ctime>
#include <fstream>
#include <conio.h>




using namespace std;

int main()
{
	std::cout.sync_with_stdio(false);

	const unsigned short port = 0u;
	const unsigned baud = 115200u;
	RS232::SerialConnection serConn;
	if (serConn.OpenPort(port, baud) == 0) //
	{

		auto myGamepad = new Gamepad(0);
		ofstream file;
		file.open("out.txt");

		while (!_kbhit())
		{
			myGamepad->update();

			if (myGamepad->isConnected()) {
				// Retrieve data
				float throttle = 0.0f;

				float lt =  myGamepad->getLTrigger();
				float rt = myGamepad->getRTrigger();

				if (lt >= rt)
					throttle = -lt;
				else
					throttle = rt;


				float steer =  myGamepad->getLStickPosition().x;
				
				unsigned char stringBuff[13]= { '\0' };
				int stringShift = 0;

				stringBuff[0] = 'Y';
				stringShift++;

				char fbuff[7];
				snprintf(fbuff, 7, "%+.3f", throttle);
				
				for (int i = 0; i < 6; i++)
				{
					if (i < 2)
						stringBuff[stringShift + i] = fbuff[i];
					else if(i > 2)
						stringBuff[stringShift + i-1] = fbuff[i];
				}
				stringShift += 5;

				stringBuff[stringShift] = 'X';
				stringShift++;

				snprintf(fbuff, 7, "%+.3f", steer);

				for (int i = 0; i < 6; i++)
				{
					if (i < 2)
						stringBuff[stringShift + i] = fbuff[i];
					else if (i > 2)
						stringBuff[stringShift + i - 1] = fbuff[i];
				}
				stringShift += 5;

				//cout << stringBuff << endl;
				if (serConn.SendByteArray(port, stringBuff, 13) == -1)
				{
					cout << "DISCONNECTED" << endl;
					break;
				}

				unsigned char recChar = '\0';
				bool readStatus = true;
				time_t  timec;
				time_t  timed;
				time(&timec);

				while (serConn.ReadByte(port, recChar) != 0) {
					time(&timed);
					if ((timed - timec) > 0.1f)
					{
						readStatus = false;
						break;
					}	
				}
				
				if (readStatus)
				{
					unsigned char buff[100];	
					
					int size = serConn.ReadByteArray(port, buff, 100); 

					char sizeBuf[3] = {'\0'};
					sizeBuf[0] = buff[0];
					sizeBuf[1] = buff[1];

					int recSize = atoi(sizeBuf);
					if (size == recSize && recSize > 0)
					{
						buff[size] = '\0';
						cout << recChar << buff << endl;

						file << recChar << buff << endl;
					}
					
					//file << recChar;
				}

				//Sleep(30);
				
			}
			else {
				cout << "GAMEPAD NOT CONNECTED" << endl;
				break;
			}
		}
		file.close();
		cout << "end" << endl;
	}
	system("pause");

}