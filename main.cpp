#include "mbed.h"
#include "Engine.h"
#include "SerialSingleton.h"
#include "OrientationComputer.h"
#include "NeatGUI.h"
#include "SHARPIR.h"
#include "DriverAlgorithm.h"



#define RAD 0.01745329252f

SHARPIR * sensorsS[5];


 int itoaLength(int n)
 {
     int i, sign;
 
     if ((sign = n) < 0)
         n = -n;
     i = 0;
     do {
         i++;
     } while ((n /= 10) > 0);
     if (sign < 0)
         i++;
		 
		 return i;
 }
float _prevAngle = 0;
 
float prevLeft = 0, prevRight = 0, prevCenter = 0, prev_side_left = 0, prev_side_right = 0;

void showSensors(Display &d, float left, float right, float center, float side_left, float side_right)
{
    d.drawCircle(215, 160, 1, 0xfff00fff);
    d.drawCircle(215, 168, 1, 0xfff00fff);
    d.drawCircle(215, 152, 1, 0xfff00fff);
		d.drawCircle(220, 168, 1, 0xfff00fff);
		d.drawCircle(220, 152, 1, 0xfff00fff);
	
	
    d.drawLine(215, 160, 215 - prevCenter, 160, 0xFF000000);
    d.drawLine(215, 160, 215 - center, 160, 0xFFFF00FF);

    d.drawLine(215, 168, 215 - prevLeft * 0.87, 164 - prevLeft * 0.5, 0xFF000000);
    d.drawLine(215, 168, 215 - left * 0.87, 164 - left * 0.5, 0xFF00FFFF);

    d.drawLine(215, 152, 215 - prevRight * 0.87, 164 + prevRight * 0.5, 0xFF000000);
    d.drawLine(215, 152, 215 - right * 0.87, 164 + right * 0.5, 0xFF00FFFF);

    d.drawLine(220, 152, 220, 152 - prev_side_right, 0xFF000000);
    d.drawLine(220, 152, 220, 152 - side_right, 0xFFFF00FF);
	
		d.drawLine(220, 168, 220, 168 + prev_side_left, 0xFF000000);
		d.drawLine(220, 168, 220, 168 + side_left, 0xFFFFFF00);


    d.drawLine(215 - prevCenter, 160, 215 - prevLeft * 0.87, 164 - prevLeft * 0.5, 0xFF000000);
    d.drawLine(215 - center, 160, 215 - left * 0.87, 164 - left * 0.5, 0xFF00FFFF);

    d.drawLine(215 - prevCenter, 160, 215 - prevRight * 0.87, 164 + prevRight * 0.5, 0xFF000000);
    d.drawLine(215 - center, 160, 215 - right * 0.87, 164 + right * 0.5, 0xFF00FFFF);

		d.drawLine(220, 152 - prev_side_right, 215 - prevLeft * 0.87, 164 - prevLeft * 0.5, 0xFF000000);
    d.drawLine(220, 152 - side_right, 215 - left * 0.87, 164 - left * 0.5, 0xFF00FFFF);

    d.drawLine(220, 168 + prev_side_left, 215 - prevRight * 0.87, 164 + prevRight * 0.5, 0xFF000000);
    d.drawLine(220, 168 + side_left, 215 - right * 0.87, 164 + right * 0.5, 0xFF00FFFF);

    prevCenter = center;
    prevLeft = left;
    prevRight = right;
		prev_side_left = side_left;
		prev_side_right = side_right;
}
 
 
int main()
{	
	//DigitalOut backlight1(TFT_LED);
	//DigitalOut reset(TFT_RST);
	//backlight1 = 1;
	//reset = 1;
	
	SS::get().baud(115200);
	SS::get().printf("Alive\r\n");

	Engine en;
	en.setMode(Engine::PWM);
	en.setSpeed(0.0f);
	en.steer(0.0f);
	
	OrientationComputer comp;
	comp.init(MPU6050::AFS_16G, MPU6050::GFS_2000DPS);
	wait(1.5);

	//ILI9341 d(TFT_MOSI, TFT_MISO, TFT_SCK, TFT_CS, TFT_RS);

	//d.open();
	//d.state(Display::DISPLAY_ON);
	//d.clear();
	
	
	DigitalOut led(LED1);
	led = 1;
	
	sensorsS[FRONT_CENTER] = new SHARPIR(IRD2);
	sensorsS[MIDDLE_LEFT] =  new SHARPIR(IRD4);
	sensorsS[MIDDLE_RIGHT] = new SHARPIR(IRD5);
	sensorsS[FRONT_RIGHT] = new SHARPIR(IRD1);
	sensorsS[FRONT_LEFT] = new SHARPIR(IRD3);
	
	DriverAlgorithm algo(&en,sensorsS, comp);

	en.setMode(Engine::PID);
	en.setSpeed(0.0f);
	en.steer(0.0f);

	float counter = 0;

	while(1)
	{
		//showSensors(d, sensorsS[FRONT_LEFT]->cm(), sensorsS[FRONT_RIGHT]->cm(), sensorsS[FRONT_CENTER]->cm(), sensorsS[MIDDLE_LEFT]->cm(), sensorsS[MIDDLE_RIGHT]->cm());

		comp.update();
		//en.setSpeed(2.0f);
		algo.Tick(0);
		wait(0.001);
	}	
}
