#include "Motor.h"

Motor::Motor(PinName in1, PinName in2, PinName pwm, PinName feedback, PinName fault) : 
	_in1(in1), 
	_in2(in2), 
	_pwm(pwm), 
	_fb(feedback), 
	_fault(fault)
{
	_pwm.period(1.0f/15000.0f);
	_pwm = 0.0f;
	_speed = 0.0f;
	_in1 = 0;
	_in2 = 0;
	
	_fault.fall(this, &Motor::handleFailure);
}

void Motor::setPWM(float speed) 
{	
	_speed = speed;
	if(speed > 0.99f) speed = 0.99f;
	if(speed <-0.99f) speed =-0.99f;
	
	if(speed > 0.0f)
	{		
		_in1 = 0;
		_in2 = 1;
	}
	else if(speed < 0.0f)
	{
		_in1 = 1;
		_in2 = 0;
	}
	else
	{
		_in1 = 0;
		_in2 = 0;
		speed = 0.0f;
	}

	_pwm.write(fabs(speed));
}

float Motor::getPWM()
{
	return _speed;
}

float Motor::getFeedback() 
{
	return _fb.read();
}

bool Motor::getFailure()
{
	return _fault.read();
}

void Motor::handleFailure()
{
	_in1 = 0;
	_in2 = 0; 
	wait_us(20);
	
	_in1 = 1;
	_in2 = 1;
	wait_us(20);
	
	_in1 = 0;
	_in2 = 0; 		
	_pwm.write(0.0f);
	_fault.mode(PullDown);	
	wait_us(20);
	
	_fault.mode(PullNone);	
}

