#if !defined(_SPEEDSENSOR_H)
#define _SPEEDSENSOR_H

#include "mbed.h"
#include "Constants.h"
/**
 * Quadrature Encoder Interface.
 */
class SpeedSensor {
public: 
	SpeedSensor(PinName channelA, PinName channelB, float pulsesPerRev);

	void reset();

	int getPulses();

	float getDistance();
	
	float getSpeed();

private:

	/**
	 * Update the pulse count.
	 *
	 * Called on every rising/falling edge of channel A.
	 *
	 * Reads the state of the channels and determines whether a pulse forward
	 * or backward has occured, updating the count appropriately.
	 */
	void encode();

	void updateSpeed();

	void resetSpeed();

	InterruptIn channelA_;
	DigitalIn channelB_;

	Timer time;
	Ticker watchdog;

	int   prevState_;
	int   currState_;
	float speed_;
	float prev_speed;
	float pulsesPerRev_;
	float length_per_pulse;
	float wheel_length;
	
	float dt;
		
	volatile int pulses_;
	volatile int speed_sign;
	volatile int revolutions_;
	
	volatile int prev_pin_state;
};

#endif  //_SPEEDSENSOR_H