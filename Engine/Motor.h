#if !defined(_MOTOR_H)
#define _MOTOR_H
#include "mbed.h"

class Motor 
{
public:
	Motor(PinName in1, PinName in2, PinName pwm, PinName feedback, PinName fault);
	
public:
	void  setPWM(float speed);
	float getPWM();

	float getFeedback();
	bool  getFailure();

protected:
	void handleFailure();

	float _speed;
	DigitalOut _in1, _in2;	
	PwmOut _pwm;	
	AnalogIn _fb;
	InterruptIn _fault;
};

#endif  //_MOTOR_H
