#include "Engine.h"
#include "SerialSingleton.h"

const float Engine::servoMountOffset = 5.5f;
const float Engine::dt = 0.075f;

Engine::Engine() : steerServo(SERVO_OUT), 
				   motor(MOTOR2_IN1, MOTOR2_IN2, MOTOR2_PWM, MOTOR2_CURRENT, MOTOR2_FAULT),
				   sensor(SPEED_A_IN, SPEED_B_IN, Constants::PULSES_PER_REV),
				   pid(0.3f, 0.3f, 6.1e-5f, dt)
{
	loopAttached = false;
	setMode(Engine::PID);
	
	engineEnabled = false;	
	
	pid.setConstrains(-1.0f, 1.0f);
	pid.setSetpoint(0.0f);
	
	currentAngle = 0.0f;
	steer(currentAngle);
}

void Engine::setSpeed(float speed)
{
	switch(_mode)
	{
		case PID:
			setpoint = speed; break;
		case PWM:
		{
			if(speed > 1.0f) speed = 1.0f;
			if(speed < -1.0f) speed = -1.0f;
			motor.setPWM(speed);
		}; break;
		default: {};
	}
}

float Engine::getSpeed()
{
	return sensor.getSpeed();
}
float Engine::getDistance()
{
	return sensor.getDistance();
}

void Engine::setMode(int mode) 
{ 
	_mode = mode;
	switch(_mode)
	{
		case PID:
		{
			if(!loopAttached)
			{
				feedback.attach(this, &Engine::feedbackLoop, dt);
				loopAttached = true;
			}
		}; break;
		case PWM:
		{
			if(loopAttached)
			{
				feedback.detach();
				loopAttached = false;
			}
		}; break;
		default: {};
	}
};
int Engine::getMode()
{ 
	return _mode;
} 

float Engine::getActualMotorPWM()
{
	return motor.getPWM();
}

float Engine::getFeedback()
{
	return motor.getFeedback();
}

float Engine::getYawRate()
{
	return getSpeed() * sin(currentAngle / (180 / PI)) / Constants::WHEEL_BASE * (180 / PI);
}

void Engine::steer(float angle) 
{
	if(angle > servoMax) angle = servoMax;
	if(angle < servoMin) angle = servoMin;
	
	currentAngle = angle;
	
	angle += servoMountOffset;

	steerServo.position(angle);
}

float Engine::getCurrentAngle()
{
	return currentAngle;
}

int Engine::getFailure() 
{
	return motor.getFailure();
}

void Engine::feedbackLoop()
{
	float speed, output;

	pid.setSetpoint(setpoint);
	speed = sensor.getSpeed();
	
	if(setpoint == 0 && fabs(speed) < 0.1) 
			pid.softReset(); 
		
	output = pid.computeOutput(speed);				
	motor.setPWM(output);
}

Engine::~Engine()
{

}
