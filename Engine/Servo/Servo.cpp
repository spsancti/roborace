#include "Servo.h"
#include "mbed.h"
 
Servo::Servo(PinName pin) : servoPin(pin), _position(0) {}

void Servo::position(float pos) 
{
		if(pos < 0.0f || pos > 180.0f) 
			return;
	
		_position = (int)(pos * 10.0f) + 500;
}

void Servo::startPulse() 
{
		servoPin = 1;
		pulseStop.attach_us(this, &Servo::endPulse, _position);
}

void Servo::endPulse() 
{
		servoPin = 0;
}

void Servo::enable() 
{
		position(0.0f);
		pulse.attach_us(this, &Servo::startPulse, 20000);
}

void Servo::disable() 
{
		pulse.detach();
}