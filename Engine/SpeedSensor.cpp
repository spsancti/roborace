#include "SpeedSensor.h"

SpeedSensor::SpeedSensor(PinName channelA, PinName channelB, float pulsesPerRev) : channelA_(channelA), channelB_(channelB), pulsesPerRev_(pulsesPerRev)
{
	pulses_     = 0;
	revolutions_= 0;
	speed_ 		= 0;
	prev_speed  = 0;
	speed_sign = 0;
	dt = 0.05f;
	
	wheel_length = 2 * PI * Constants::WHEEL_RADIUS;
	length_per_pulse = 1.0f / pulsesPerRev_ * wheel_length;	
	
	//Workout what the current state is.	
	int chanA = channelA_.read();
	int chanB = channelB_.read();
	
	//2-bit state.
	currState_ = (chanA << 1) | (chanB);
	prevState_ = currState_;

	prev_pin_state = chanA;
	
	channelA_.rise(this, &SpeedSensor::encode);
	channelA_.fall(this, &SpeedSensor::encode);

	time.start();
	watchdog.attach(this, &SpeedSensor::resetSpeed, 0.05f);
}
 
void SpeedSensor::reset() 
{
	pulses_     = 0;
	revolutions_= 0;
	speed_ 		= 0;
	prev_speed  = 0;
	speed_sign = 0;
	dt = 0.05f;
}
 
int SpeedSensor::getPulses() 
{ 
	return pulses_; 
}
 
float SpeedSensor::getDistance() 
{ 
	return ((float)pulses_ / pulsesPerRev_) * wheel_length; 
}
 /*
// +-------------+
// | X2 Encoding |
// +-------------+
//
// When observing states two patterns will appear:
//
// Counter clockwise rotation:
//
// 10 -> 01 -> 10 -> 01 -> ...
//
// Clockwise rotation:
//
// 11 -> 00 -> 11 -> 00 -> ...
//
// We consider counter clockwise rotation to be "forward" and
// counter clockwise to be "backward". Therefore pulse count will increase
// during counter clockwise rotation and decrease during clockwise rotation.
//
// +-------------+
// | X4 Encoding |
// +-------------+
//
// There are four possible states for a quadrature encoder which correspond to
// 2-bit gray code.
//
// A state change is only valid if of only one bit has changed.
// A state change is invalid if both bits have changed.
//
// Clockwise Rotation ->
//
//    00 01 11 10 00
//
// <- Counter Clockwise Rotation
//
// If we observe any valid state changes going from left to right, we have
// moved one pulse clockwise [we will consider this "backward" or "negative"].
//
// If we observe any valid state changes going from right to left we have
// moved one pulse counter clockwise [we will consider this "forward" or
// "positive"].
//
// We might enter an invalid state for a number of reasons which are hard to
// predict - if this is the case, it is generally safe to ignore it, update
// the state and carry on, with the error correcting itself shortly after.*/
void SpeedSensor::encode() 
{ 
	int chanA  = channelA_.read();
	int chanB  = channelB_.read();
	
	//2-bit state.
	currState_ = (chanA << 1) | (chanB);

	//11->00->11->00 is counter clockwise rotation or "forward".
	if ((prevState_ == 0x3 && currState_ == 0x0) ||
		(prevState_ == 0x0 && currState_ == 0x3)) 
	{
		pulses_++;		
		speed_sign = 1;
	}
	//10->01->10->01 is clockwise rotation or "backward".
	else if ((prevState_ == 0x2 && currState_ == 0x1) ||
			 (prevState_ == 0x1 && currState_ == 0x2)) 
	{
		pulses_--;		
		speed_sign = -1;
	}	
	prevState_ = currState_; 
		
	//update the speed
	dt = time.read();
	time.reset();
	
	if(dt < 0.0015f) 
		return;
		
	speed_ = 0.3f * (length_per_pulse / dt * speed_sign) + 0.7f * prev_speed;
	
	prev_speed = speed_;
}

void SpeedSensor::resetSpeed()
{
	if(time.read_ms() > 100)
	{
		speed_ = 0.0f;
	}
}

float SpeedSensor::getSpeed() 
{	
	return speed_;
}