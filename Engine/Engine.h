#if !defined(_ENGINE_H)
#define _ENGINE_H

#include "mbed.h"
#include "Motor.h"
#include "ServoPwm.h"
#include "peripherals.h"
#include "SpeedSensor.h"
#include "MPID.h"


class Engine {
public:
	Engine();
	~Engine();
	void setSpeed(float speed);

	float getSpeed();
	float getDistance();


	void setMode(int mode);
	int getMode();

	float getActualMotorPWM();

	float getFeedback();
	
	float getYawRate();

	void steer(float angle);
	float getCurrentAngle();

	int getFailure();

public:
	enum MODE {
		PWM,
		PID
	};

private:
	Ticker feedback;
	static const float dt;

	void feedbackLoop();

public:	
	static const float servoMountOffset;
	static const int servoMax = 19;
	static const int servoMin = -19;
	ServoPWM steerServo;
	float currentAngle;	

private:
	float setpoint;
	Motor motor;
	SpeedSensor sensor;
	MPID pid;	
	bool engineEnabled;
	int _mode;
	bool loopAttached;
};

#endif  //_ENGINE_H*/
