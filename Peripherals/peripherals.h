#ifndef DEFINES_H_
#define DEFINES_H_

#define TFT_LED 	PE_7
#define TFT_CS 		PD_11
#define TFT_MOSI	PB_15
#define TFT_MISO	PB_14
#define TFT_SCK		PB_13
#define TFT_RST		PD_9
#define TFT_RS		PD_15

#define MOTOR1_PWM		PC_9
#define MOTOR1_IN1		PA_9
#define MOTOR1_IN2		PA_10
#define MOTOR1_FAULT 	PA_8
#define MOTOR1_CURRENT	PC_0

#define MOTOR2_PWM		PB_8
#define MOTOR2_IN1		PC_13
#define MOTOR2_IN2		PC_15
#define MOTOR2_FAULT 	PC_14
#define MOTOR2_CURRENT 	PC_1

#define SERVO_OUT		PF_9
#define ESC_OUT			PF_10

#define BATTERY_VIN		PC_2

#define SONAR_TRIG		PC_7
#define SONAR_ECHO		PC_6

#define IRSENSOR_FL		PB_12
#define IRSENSOR_FC		PD_8
#define IRSENSOR_FR		PD_10
#define IRSENSOR_RL		PD_12
#define IRSENSOR_RR		PD_14

#define SPEED_A_IN		PC_4
#define SPEED_B_IN		PC_5

#define LSM303_SDA		PB_7
#define LSM303_SCL		PB_6
#define LSM303_INT1		PE_4
#define LSM303_INT2		PE_5
#define LSM303_DRDY		PE_2

#define L3GD20_SPI		PE_3
#define L3GD20_MOSI		PA_7
#define L3GD20_MISO		PA_6
#define L3GD20_SCK		PA_5

#define HEADLIGHT 		PF_4
#define REAR_STOP_LED	PB_4
#define RL_TURN_LED		PD_7
#define RR_TURN_LED		PB_5

#define BUTT_UP			PB_0
#define BUTT_DOWN		PB_2
#define BUTT_OK			PB_1

#define RX SERIAL_RX
#define TX SERIAL_TX
#define BAUDRATE 115200

#define IRD1				   PD_14
#define IRD2                   PD_12
#define IRD3                   PD_10
#define IRD4                   PD_8
#define IRD5                   PB_12

#define I2C_SDA				   PA_1
#define I2C_SCL				   PC_3

#define RAD2DEG 	57.295779513f

#endif //DEFINES_H_
