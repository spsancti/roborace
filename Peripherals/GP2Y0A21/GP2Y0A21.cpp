#include "GP2Y0A21.h"


GP2Y0A21::GP2Y0A21(PinName adc): _adc(adc)
{
	
}

float GP2Y0A21::read_raw()
{
		return _adc.read();
}

float GP2Y0A21::read()
{
	float x = read_raw();
	if(x > 0.7f)  return 0;
	if(x < 0.17f) return 0.5;
	
	return  8.6540864234217207e-001 * pow(x,0)
        + -2.7571752005964822e+000 * pow(x,1)
        +  2.1870817818612434e+000 * pow(x,2);
}