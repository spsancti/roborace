#ifndef GP2Y0A2F_H_
#define GP2Y0A2F_H_

#include "mbed.h"

class GP2Y0A2F
{
public:
	GP2Y0A2F(PinName adc);
	
	float read();
	float read_raw();
	
#ifdef MBED_OPERATORS
    operator float() { return read(); }
#endif

private:
	AnalogIn _adc;
};


#endif
