#ifndef SERIALSINGLETON_H_
#define SERIALSINGLETON_H_
#include "mbed.h"
#include "peripherals.h"

class SerialSingleton
{
	private :
		SerialSingleton();		
	
	public:
   static Serial& get()
   {
      static Serial INSTANCE(TX, RX);
      return INSTANCE;
   }
};

typedef SerialSingleton SS; 

#endif //SERIALSINGLETON_H_
