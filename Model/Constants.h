#if !defined(_CONSTANTS_H)
#define _CONSTANTS_H

#undef PI
#define PI 3.1415926535897932384626433832795028842f //we need loooong PI, because we integrate it


class Constants {
public:
	static const float LENGTH;
	static const float CAR_MASS;
	static const float WHEEL_RADIUS;
	static const float WHEEL_BASE;
	static const float WHEEL_DIST;
	static const float PULSES_PER_REV;

	static const float ABS_SPEED_LEVEL;


};

#endif  //_CONSTANTS_H
