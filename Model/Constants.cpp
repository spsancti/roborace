#include "Constants.h"

const float Constants::CAR_MASS = 0.817f;
const float Constants::WHEEL_RADIUS = 0.03f;
const float Constants::WHEEL_BASE = 0.1981f;
const float Constants::WHEEL_DIST = 0.1397f;

const float	Constants::PULSES_PER_REV = 17.4;

const float Constants::ABS_SPEED_LEVEL = -1.5f; //TODO: Tune to track stiffness

const float Constants::LENGTH = Constants::WHEEL_BASE + 3*Constants::WHEEL_RADIUS;
