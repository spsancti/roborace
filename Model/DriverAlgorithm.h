#include "Engine.h"
#include "SHARPIR.h"
#include "OrientationComputer.h"
#include <vector>




class FVector2D
{
public:
	FVector2D() {}
	FVector2D(float X, float Y) : X(X), Y(Y) {}
	float X;
	float Y;
};

class SensorPoint {
public:
	FVector2D coord;
	float rad;
	SensorPoint() {}
	SensorPoint(const FVector2D & coord) : coord(coord),
		rad(sqrtf(powf((coord.X - 0), 2) + powf((coord.Y - 0), 2))) {}

	const bool operator < (const SensorPoint &sp) const {
		return (rad < sp.rad);
	}
};

enum SensorLocation {
		MIDDLE_LEFT = 0,
		FRONT_RIGHT,
		FRONT_CENTER,
		FRONT_LEFT,
		MIDDLE_RIGHT
};

class DriverAlgorithm
{
public:
	DriverAlgorithm(Engine * en, SHARPIR * sensorsS[5], OrientationComputer & com);
	void Tick(float Delta);
private:
	float _angle;
	float _prevAngle;

	FVector2D carSize;

	SHARPIR * sensors[5];
	Engine * engine;
	float _speed;
	Vector3 gyro;
	Vector3 accel;

	float sensorsValues[5];
	SensorPoint points[5];

	void cookSensors();
	float calcDirection();

	OrientationComputer & com;
	int maxSensor;

	float calcSlipAccelX(float angle, float s, float accel);
	float distToBorder(float angle);
	
	float calculateArea();

	void addPointsToMiddle(const FVector2D & a, const FVector2D & b, float offset, std::vector<FVector2D>& out)
	{
		if (decDistance(a, b) >= offset) {
			FVector2D middle = getMiddlePoint(a, b);

			addPointsToMiddle(a, middle, offset, out);
			out.push_back(middle);
			addPointsToMiddle(middle, b, offset, out);
		}
	}
	bool bSlipFailure;
	float _prevSpeed;
	
	FVector2D getMiddlePoint(const FVector2D & a, const FVector2D & b)
	{
		return FVector2D((a.X + b.X) / 2.0f, (a.Y + b.Y) / 2.0f);
	}

	float decDistance(FVector2D a1, FVector2D a2)
	{
		return sqrtf(powf((a2.X - a1.X), 2) + powf((a2.Y - a1.Y), 2));
	}

	template <class T>
	bool isInRange(const T & value, const T & min, const T & max) {
		return ((value <= max) && (value >= min));
	}


};