#include "DriverAlgorithm.h"
#include "Constants.h"

#define RAD 0.0174533f

#define sign(x) ((x > 0) - (x < 0))


#define A_L 0.1f
#define B_L 0.1f
#define C_Y 2.0555
#define M  0.6f
#define MAX_ANGLE 19.0f
#define SENSOR_LENGTH 151.0f
#define BASE_L 18.0f
#define FRICTION 3.0f
//#define MAX_SPEED 2.0f

static float clamp(float value, float min, float max) {
    if(value < min) {
        return min;
    } else if(value > max) {
        return max;
    } else {
        return value;
    }
}

DriverAlgorithm::DriverAlgorithm(Engine * en, SHARPIR * sensors[5], OrientationComputer & com)
	: engine(en), com(com)
{
	for (int i = MIDDLE_LEFT; i <= MIDDLE_RIGHT; i++)
	{
			this->sensors[i] = sensors[i];
	}
	_angle = 0.0f;
	_prevAngle = 0.0f;
	carSize = FVector2D(20.0f, 30.0f);
	_prevSpeed = 0.0f;
	maxSensor = 0;
	
}

void DriverAlgorithm::cookSensors()
{
	for (int i = MIDDLE_LEFT; i <= MIDDLE_RIGHT; i++)
	{
		sensorsValues[i] = sensors[i]->cm();
	}
	
	if ((sensorsValues[MIDDLE_LEFT]) < 20.0f)
		sensorsValues[MIDDLE_LEFT] = 20.0f;
	
	if ((sensorsValues[MIDDLE_RIGHT]) < 20.0f)
		sensorsValues[MIDDLE_RIGHT] = 20.0f;

	points[FRONT_CENTER] = FVector2D(0, sensorsValues[FRONT_CENTER]);
	points[MIDDLE_LEFT] = FVector2D(-sensorsValues[MIDDLE_LEFT], 12.0f);// - carSize.X / 2
	points[MIDDLE_RIGHT] = FVector2D(sensorsValues[MIDDLE_RIGHT], 12.0f); 
	points[FRONT_LEFT] = FVector2D(((sensorsValues[FRONT_LEFT] * cosf(60.0f * RAD)) + 8.0f), sensorsValues[FRONT_LEFT] * sinf(60.0f * RAD));
	points[FRONT_RIGHT] = FVector2D(-((sensorsValues[FRONT_RIGHT] * cosf(60.0f * RAD))+ 8.0f), sensorsValues[FRONT_RIGHT] * sinf(60.0f * RAD));
	
	com.getAccAtCarFrame(accel);
	com.getGyroAtCarFrame(gyro);
}

float DriverAlgorithm::calculateArea()
{
	int point = 5;
	float  area = 0.0f ;
	int i, j = point-1  ;

	for (i = 0; i < point; i++) 
	{
		area += (points[j].coord.X + points[i].coord.X)*(points[j].coord.Y - points[i].coord.Y); 
		j = i; 
	}

	return area * 0.5f * 10e-6f;
}


float DriverAlgorithm::calcDirection()
{
	SensorPoint * result = &points[0];
	for (int i =1;i < 5;i++)
	{
		if ((points + i)->rad > result->rad)
		{
			result = (points + i);
			maxSensor = i;
		}
	}
	
	int infinity[5] = {0};
	int sensorWeight[5] = {4,3,1,3,4};
	
	for (int i = 0;i < 5;i++)
	{
		if ((points + i)->rad == 151)
		{
			infinity[i] = sensorWeight[i];
		}
	}
	
	int infinityMax = -1;
	if (infinity[0] > 0)
		infinityMax = 0;
	
	for (int i = 1;i < 5;i++)
	{
		if (infinity[i] > 0)
		{
			if (infinity[i] > infinity[i-1])
			{	
				infinityMax = i;
			}
		}
	}
	
	if (infinityMax != -1)
	{
			result = (points + infinityMax);
			maxSensor = infinityMax;
	}
	
	SensorPoint * startPoint;
	SensorPoint * endPoint;

	if (result == (points)) {
		startPoint = result;
		endPoint = (result + 1);
	}
	else if (result == (points+5)) {
		startPoint = (result - 1);
		endPoint = result;
	}
	else {
		startPoint = (result - 1);
		endPoint = (result + 1);
	}

	FVector2D middlePoint((startPoint->coord.X + endPoint->coord.X) / 2, (startPoint->coord.Y + endPoint->coord.Y) / 2);	
	float angle = atan2(middlePoint.Y, middlePoint.X) * 180.0f / PI - 90.0f;

	return angle;
}

float DriverAlgorithm::calcSlipAccelX(float angle, float s, float accel)
{
	float m = M;

	float dS = accel;
	float Ffr, Ffl;

	Ffr = m * dS / cosf(angle * RAD);
	Ffl = Ffr;

	float x1 = s * cosf( - gyro.z * RAD);
	float x2 = s * sinf( - gyro.z * RAD);
	float x3 = - gyro.z * RAD;
	float Cy = C_Y;

	float a = A_L;
	float b = B_L;
	
	float diffAngle = angle;// -_prevAngle;
	diffAngle = diffAngle * RAD;
//((Ffr + Ffl)*sinf(diffAngle)) + //-x1*x3 + 
	float dVy = (((Ffr + Ffl)*sinf(diffAngle)) + (2 * 0.05f *(diffAngle - (x2 + a*x3) / x1)*cosf(diffAngle)) + (2 * 0.05f *(b*x3 - x2) / x1)) / m;
	if (x1 != 0.0f)
		return dVy;
	else
		return 0.0f;
}

void DriverAlgorithm::Tick(float Delta)
{
	bSlipFailure = false;

	cookSensors();
	_angle = calcDirection();
	
	if ((sensorsValues[MIDDLE_LEFT] < 30.0f && _angle > 0) || (sensorsValues[MIDDLE_RIGHT] < 30.0f && _angle < 0)) {
		bool bIsRight;
		float lowerDist;
		bIsRight = (sensorsValues[MIDDLE_RIGHT] < 30.0f) ? true : false;
		lowerDist = sensorsValues[bIsRight ? MIDDLE_RIGHT : MIDDLE_LEFT] - 20.0f;
		_angle = _angle * (((lowerDist >= 0) ? lowerDist : 0) / (30.0f - 20.0f)) / 2.5f;
	}
	
	_angle = clamp(_angle, -19.0f, 19.0f);

	float cSpeed = engine->getSpeed();
	float cAccel = - accel.y * 9.81f;
	
	float MAX_SPEED = 2.0f;

		
		float slipAccelX = calcSlipAccelX(_angle, cSpeed, cAccel);

	if (fabs(slipAccelX) < FRICTION) 
		{
			_speed = MAX_SPEED;
		}
		else 
		{
			//Compute angle need to not crash
			float needToGoAngle = _angle;
			while (distToBorder(needToGoAngle) < 0 && fabs(needToGoAngle) < MAX_ANGLE)
			{
				needToGoAngle = needToGoAngle + sign(needToGoAngle) * 1.0f;
			}
			
			//Compute minimal avalible sleep angle by current speed and angle needed to not crash
			float noSlipAngle = floorf(_angle);
			float minAvalibleSlipAccelX = slipAccelX;
			while (noSlipAngle != 0 && fabs(minAvalibleSlipAccelX) > FRICTION)
			{
				noSlipAngle = noSlipAngle -sign(_angle) * 1.0f;
				minAvalibleSlipAccelX = calcSlipAccelX(noSlipAngle, cSpeed, cAccel);
			}


			if (fabs(minAvalibleSlipAccelX) < FRICTION)
			{
				if (fabs(noSlipAngle) >= fabs(needToGoAngle))
				{
					_angle = noSlipAngle;
					_speed = MAX_SPEED;

				}
				else
				{
					_angle = needToGoAngle;

					float nSpeed = cSpeed;
					float tSleepAx = calcSlipAccelX(needToGoAngle, nSpeed, cAccel);

					while (nSpeed > 0 && fabs(tSleepAx) > FRICTION)
					{
						nSpeed -= 0.01f;
						tSleepAx = calcSlipAccelX(needToGoAngle, nSpeed, cAccel);
					}

					if (calcSlipAccelX(needToGoAngle, cSpeed, nSpeed - cSpeed) < FRICTION)
					{
						_speed = nSpeed;
					}
					else
					{
						bSlipFailure = true;
					}
				}
			}
			else
			{
				_angle = needToGoAngle;

				float nSpeed = cSpeed;
				float tSleepAx = calcSlipAccelX(needToGoAngle, nSpeed, cAccel);

				while (nSpeed > 0 && fabs(tSleepAx) > FRICTION)
				{
					nSpeed -= 0.01f;
					tSleepAx = calcSlipAccelX(needToGoAngle, nSpeed, cAccel);
				}

				if (calcSlipAccelX(needToGoAngle, cSpeed, nSpeed - cSpeed) < FRICTION)
				{
					_speed = nSpeed;
				}
				else
				{	
					bSlipFailure = true;
				}
			}
		}
		
		if(calculateArea() < 0.01f)
		{
			engine->steer(-engine->getCurrentAngle());
			engine->setSpeed(-1.5f);
			wait(0.05);
		}

		engine->steer(_angle);
		_prevAngle = _angle;

		if (!bSlipFailure)
		{
			engine->setSpeed(_speed);
			_prevSpeed = _speed;
		}			
		else
		{
			engine->setSpeed(engine->getSpeed()/3);
		}
}

float DriverAlgorithm::distToBorder(float angle)
{
	float disDiff = SENSOR_LENGTH;
	float L = BASE_L;

	angle = -angle;

	if (fabs(angle) > 1.0f)
	{
		float r = L / sinf(angle * RAD);
		
		for (int i = MIDDLE_LEFT + 1; i < MIDDLE_RIGHT; i++)
		{
			if (!((i == maxSensor) || ((i-1) == maxSensor)) && (maxSensor != FRONT_CENTER)
				&& (sign(points[i].coord.X) == sign(angle) || sign(points[i-1].coord.X) == sign(angle))
				)
			{
				std::vector<FVector2D> out;
				FVector2D a11(points[i - 1].coord.X, points[i - 1].coord.Y);
				FVector2D b22(points[i].coord.X, points[i].coord.Y);
				addPointsToMiddle(a11, b22, 10.0f, out);

				for (int z = 1; z < out.size(); z++)
				{

					float x21 = r;
					float y21 = -12.0f;

					float d = 0.0f;
			
					r = r + sign(r) * d;

					FVector2D middle((out.at(z - 1).X + out.at(z).X) / 2.0f, (out.at(z - 1).Y + out.at(z).Y) / 2.0f);
					float checkAngle = atan2f(middle.Y, middle.X);

					float x22 = x21 + r * cosf(checkAngle);
					float y22 = r * sinf(checkAngle);

					float k2 = (y22 - y21) / (x22 - x21);

					float x11 = out.at(z - 1).X;
					float y11 = out.at(z - 1).Y;

					float x12 = out.at(z).X;
					float y12 = out.at(z).Y;

					float k1 = (y12 - y11) / (x12 - x11);

					float intsX = (-y11 + k1 * x11 + y21 - k2 * x21) / (k1 - k2);
					float intsY = k2 * intsX + y21 - k2 * x21;

					float borderDist = decDistance(FVector2D(x21, y21), FVector2D(intsX, intsY));
					float radDist = decDistance(FVector2D(x21, y21), FVector2D(x22, y22));

					if ( intsY >= -13.0f && fabs(intsX) <= SENSOR_LENGTH) //intsY >= -13.0f && 
					{
						if (borderDist - radDist < disDiff)
							disDiff = borderDist - radDist;
					}
				}
			}
		}
	}
	return disDiff;
}